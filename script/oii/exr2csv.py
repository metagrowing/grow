#!/usr/bin/env python3
import sys
import OpenImageIO as oiio
from OpenImageIO import ImageInput, ImageOutput
from OpenImageIO import ImageBuf, ImageSpec, ImageBufAlgo

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

def exr2csv (filename) :
    input = ImageInput.open (filename)
    if not input :
        eprint( 'Could not open "' + filename + '"')
        eprint( "\tError: ", oiio.geterror())
        return
    spec = input.spec()
    eprint(spec.width, spec.height, spec.nchannels)
    nchans = spec.nchannels
    pixels = input.read_image()
    if not pixels.any() :
        eprint( "Could not read:", input.geterror())
        return
    input.close()
    if spec.nchannels == 3:
      minval = [ sys.float_info.max,  sys.float_info.max,  sys.float_info.max ]
      maxval = [-sys.float_info.min, -sys.float_info.min, -sys.float_info.min ]
    elif spec.nchannels == 4:
      minval = [ sys.float_info.max,  sys.float_info.max,  sys.float_info.max,  sys.float_info.max ]
      maxval = [-sys.float_info.min, -sys.float_info.min, -sys.float_info.min, -sys.float_info.min ]
    else:
      eprint( "expected 3 or 4 channels: ", spec.nchannels)
      return
    sys.stdout.write(str(spec.width) + "," + str(spec.height) + "," + str(3) + "\n")
    for y in range(spec.height) :
        for x in range(spec.width) :
            p = pixels[y][x]
            sys.stdout.write(str(p[0]) + "," + str(p[1]) + "," + str(p[2]) + "\n")
            #if y < 1 and x < 10: eprint(p, minval, maxval, p < minval, p > maxval)
            for c in range(nchans) :
                if p[c] < minval[c] :
                    minval[c] = p[c]
                if p[c] > maxval[c] :
                    maxval[c] = p[c]
    eprint( "Min values per channel were", minval)
    eprint( "Max values per channel were", maxval)

exr2csv (sys.argv[1])
