# run inside Blender
# mark all edges of all objects as FreeStyle edges
for o in D.collections[1].objects:
  o.select_set(True)
  bpy.ops.object.editmode_toggle()
  bpy.ops.mesh.select_all(action='SELECT')
  bpy.ops.mesh.mark_freestyle_edge(clear=False)
  bpy.ops.object.editmode_toggle()
  o.select_set(False)

