# Grow

A generator for complex geometry. Used in conjunction with Blender or OpenVDB.
Based on Clojure and Stringtemplate.

Experimental code. Please do not use in production projects.

![](./grow/samples/bpy/genuary2025/gen20-generative-architecture/Screenshot_quad_field_noise.jpg)

## Documentation
Some presentation slides can be found [here](doc/grow_intro.pdf).

## Sytem requirements
- Clojure 1.10.0
- openjdk-11-jre
- Assumes that `/dev/shm` is a ram disk and is writable.

## Optional tools
- Blender 2.8x or Blender 2.79 from [blender.org](https://www.blender.org/)

## Warning
This is a code generator producing executable scripts. Please inspect the generated script code.

Grow rules can produce a huge amount of objects. Maybe your computer has to less memory to handle all this objects inside Blender.

## Expanding the rules for Blender
This [bpy](bpy) command expands your rules in `umbel1.clj` to generate an python script.
The generated python code can be found in `/dev/shm/grow-export/umbel1.clj.py`
This python scrip will be loaded as a startup script to Blender.
It will be executed to insert the geometric and color data.

```shell
./bpy grow/samples/bpy/umbel/umbel1.clj
```
## Expanding the rules for SVG
This [svg](svg) command expands your rules in `dots.clj` to generate Scalable Vector Graphics.
The generated SVG file can be found in `/dev/shm/grow-export/dots.clj.svg`
This file will be loaded into Firefox.

```shell
./svg grow/samples/svg/dots.clj
```
