(ns grow.regtest
  (:require [clojure.java.io :as io])
  (:require [clojure.string :as str])
)

(def exp-header 
"(require '[clojure.pprint :as pp]
         '[clojure.walk :as walk])")

(def exp-quote 
"(pp/pprint (walk/macroexpand-all
'(grow\n")

(def exp-footer 
"\n))\n(System/exit 0)")

(def csv-quote 
"(template-csv)")

(defn src [path]
  (with-open [rdr (io/reader path)]
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (recur (conj accu (first lines))
               (rest lines))))))

(defn exp-patch [line] (if (str/starts-with? line "(grow") exp-quote line))
(defn csv-patch [line] (if (str/starts-with? line "(template-") csv-quote line))

(defn dest [srclines patch]
  (loop [lines srclines]
    (if (empty? lines)
      :else
      (let [line (first lines)] (println (patch line)) (recur (rest lines))))))

(cond
  (zero? (compare "--exp" (first *command-line-args*))) (do
                 (println exp-header)
                 ;; (println (src "/home/strom/grow/grow/regression/coord_rotate.clj"))
                 (dest (src (second *command-line-args*)) exp-patch)
                 ;; (dest (src "/home/strom/grow/grow/regression/coord_rotate.clj"))
                 (println exp-footer)
               )
  (zero? (compare "--csv" (first *command-line-args*))) (do
                 (dest (src (second *command-line-args*)) csv-patch)
               )
  :else        (println "invalid option, use --exp or --csv"))
