(use 'grow.core)

; ----- unit testing
(require '[clojure.test :refer :all])
;(sample (mmul (translate 0.2 0.3 0.5) (mmul (translate 2 3 5) (identity-matrix))))
;(sample (mmul (scale 0.1 0.5 2) (mmul (translate 2 3 5) (identity-matrix) )))
;(sample (mmul (rotate-z (* 0.5 (Math/PI))) (identity-matrix)))
;(sample (mmul (translate 2 3 5) (mmul (rotate-z (* 0.5 (Math/PI))) (identity-matrix))))
;(sample (mmul (mmul (rotate-z (* 0.5 (Math/PI))) (identity-matrix)) (translate 2 3 5)))

;(sample (mmul (mmul (identity-matrix) (rotate-z (* 0.5 (Math/PI)))) (translate 2 3 5)))
;(sample (reduce mmul [(identity-matrix) (rotate-z (* 0.5 (Math/PI))) (translate 2 3 5)]))
(def EPSILON 0.00001)

(defmacro double= [a b]
  `(let [diff# (- ~a ~b)
         abs# (if (neg? diff#) (- diff#) diff#)]
     (< abs# EPSILON)))

(deftest test-matrix-transformation
  (is (= (mmul (mmul (identity-matrix) (rotate-z (* 0.5 (Math/PI)))) (translate 2 3 5))
         (reduce mmul [(identity-matrix) (rotate-z (* 0.5 (Math/PI))) (translate 2 3 5)]))))

(defn vector= [v w]
  (if (= (count v) (count w))
    (loop [v1 v
           w1 w
           e true]
      (if (seq v1)
        (recur (rest v1)
               (rest w1)
               (and e (double= (first v1)  (first w1))))
        e)))) 

(deftest test-color-transformation
  (is (vector= [1.0 0 1] (h+ m2Pi (hsv 1 0 1))))
  (is (vector= [1.0 0 1] (->> (hsv 1 0 1) (h+ m2Pi))))

  (is (vector= [0 0 0] (rgb 0 0 0)))
  (is (vector= cBlack  (rgb 0 0 0)))
  (is (vector= [0 0 1] (rgb 1 1 1)))
  (is (vector= cWhite  (rgb 1 1 1)))
  (is (vector= cRed    (rgb 1 0 0)))
  (is (vector= cGreen  (rgb 0 1 0)))
  (is (vector= cBlue   (rgb 0 0 1)))

  (is (vector= [0 0 0] (rgb-export (rgb 0 0 0))))
  (is (vector= [0 0 1] (rgb-export (rgb 0 0 1))))
  (is (vector= [0 1 0] (rgb-export (rgb 0 1 0))))
  (is (vector= [1 0 0] (rgb-export (rgb 1 0 0))))
  (is (vector= [1 1 1] (rgb-export (rgb 1 1 1))))

  (is (vector= [0 0 0.3] (rgb-export (rgb 0 0 0.3))))
  (is (vector= [0 0.5 0] (rgb-export (rgb 0 0.5 0))))
  (is (vector= [0.7 0 0] (rgb-export (rgb 0.7 0 0))))
  (is (vector= [0.0 0.5 0.5] (rgb-export (rgb 0.0 0.5 0.5))))
  (is (vector= [0.5 0.5 0.0] (rgb-export (rgb 0.5 0.5 0.0))))
  (is (vector= [0.5 0.5 0.5] (rgb-export (rgb 0.5 0.5 0.5))))
)

(defn range01? [v] (and (>= v 0) (<= v 1)))
                   
(defn check-noise-range [i]
  (when (pos? i)
    (is (range01? (noise [(/ i 31) 0 0] i 1)))
    (is (range01? (noise [0 (/ i 31) 0] i 1)))
    (is (range01? (noise [0 0 (/ i 31)] i 1)))
    (is (range01? (noise [(/ i 31) (/ i 31) 0] i 1)))
    (is (range01? (noise [(/ i 31) 0 (/ i 31) 0] i 1)))
    (is (range01? (noise [(* i 1.1) (* i 1.2) (* i 1.3)] 1 1)))
    (recur (dec i))))

(defn check-turbulence-range [i]
  (when (pos? i)
    (is (range01? (turbulence [(/ i 31) 0 0] i 1 5 0.95 0.995)))
    (is (range01? (turbulence [0 (/ i 31) 0] i 1 5 0.9 0.95)))
    (is (range01? (turbulence [0 0 (/ i 31)] i 1 5 0.725 0.9)))
;;     (is (range01? (turbulence [(/ i 31) (/ i 31) 0] i 1 5 0.95 0.995)))
    (is (range01? (turbulence [(/ i 31) 0 (/ i 31) 0] i 1 5 0.95 0.995)))
    (is (range01? (turbulence [(* i 1.1) (* i 1.2) (* i 1.3)] 1 1 5 0.95 0.995)))
    (recur (dec i))))

(deftest test-perlin-noise
  (check-noise-range 31)
  (check-turbulence-range 31)
)

(let [summary (run-tests)]
  (println summary)
  (System/exit (if (successful? summary) 0 1))
)
