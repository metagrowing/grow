(ns grow.core
  (:import [org.stringtemplate.v4 STGroupFile])
  (:import [org.metagrowing.noise PerlinNoise])
  (:import [java.awt Color])
  (:import [javax.imageio ImageIO])  (:require [clojure.java.io :as io])
  (:require [clojure.string :as str])
  (:require [clojure.java.shell :as shell :only [sh]])
)


(def global-traits {})

; ----- math helper
(def mSqrt2 (/ 1 (Math/sqrt 2)))
(def mPi2 (/ Math/PI 2))
(def mPi3 (/ Math/PI 3))
(def mPi4 (/ Math/PI 4))
(def mPi6 (/ Math/PI 6))
(def mPi8 (/ Math/PI 8))
(def mPi12 (/ Math/PI 12))
(def mPi24 (/ Math/PI 24))
(def mPi48 (/ Math/PI 48))
(def mPi96 (/ Math/PI 96))
(def mPi  Math/PI)
(def m2Pi (* Math/PI 2))
(def m4Pi (* Math/PI 4))
(def mGR (/ (+ 1 (Math/sqrt 5)) 2))        ; golden ratio
(def mGA (Math/abs (- (/ m2Pi mGR) m2Pi))) ; golden angel

(defn radian [deg]
  (* (/ deg 180.0) Math/PI))

(defn degree [rad]
  (* (/ rad Math/PI) 180.0))

(defn normalize2pi [h-in]
  (loop [h h-in]
    (cond
      (> h m2Pi) (recur (- h m2Pi))
      (< h 0) (recur (+ h m2Pi))
      :else h)))

(defn limit [l]
    (cond
      (> l 1) 1
      (< l 0) 0
      :else l))

(defn sign [v]
  (cond (zero? v) 0
        (> v 0)   1
        :else    -1))

(defn clamp
  "Restrict value x to a given range a b."
  [x a b]
  (min (max x a) b))

;see: https://rosettacode.org/wiki/Map_range#Clojure
(defn maprange
  "Takes two ranges and a real number, and returns the mapping of the real number from the first to the second range."
  [[a1 a2] [b1 b2] s]
	(+ b1 (/ (* (- s a1) (- b2 b1)) (- a2 a1))))

; ----- 3 element vector
; get vector components
(defn v3-x [v] (nth v 0))
(defn v3-y [v] (nth v 1))
(defn v3-z [v] (nth v 2))
(defn v3-set-x [x v] [        x (nth v 1) (nth v 2)])
(defn v3-set-y [y v] [(nth v 0)         y (nth v 2)])
(defn v3-set-z [z v] [(nth v 0) (nth v 1)         z])

; euclidian distance of this vector
(defn v3-len [v]
  (let [x (nth v 0)
        y (nth v 1)
        z (nth v 2)]
        (Math/sqrt (+ (* x x) (* y y) (* z z)))))

; subtract vector v1 from v0
(defn v3- [v0 v1]
  [(- (nth v0 0) (nth v1 0))
   (- (nth v0 1) (nth v1 1))
   (- (nth v0 2) (nth v1 2))])

; add two vectors
(defn v3+ [v0 v1]
  [(+ (nth v0 0) (nth v1 0))
   (+ (nth v0 1) (nth v1 1))
   (+ (nth v0 2) (nth v1 2))])

; multiply all vector components by a scalar
(defn v3* [s v0]
  [(* (nth v0 0) s)
   (* (nth v0 1) s)
   (* (nth v0 2) s)])

; add an offset to all vector components
(defn v3-move [offset v]
  [(+ (nth v 0) offset)
   (+ (nth v 1) offset)
   (+ (nth v 2) offset)])

; normalize vector
(defn v3-normalize [v]
  (let [x (nth v 0)
        y (nth v 1)
        z (nth v 2)
        l (Math/sqrt (+ (* x x) (* y y) (* z z)))]
    [(/ x l) (/ y l) (/ z l)]))

; ----- 4x4 element matix
; A spezialized transformation 4x4 matrix.
; Represented internally as a 16 element array in column major matrix ordering.

; elment array index for column major matrix ordering
; -           -
; | 0 4  8 12 |
; | 1 5  9 13 |
; | 2 6 10 14 |
; | 3 7 11 15 |
; -           -

; identity matrix
; -         -
; | 1 0 0 0 |
; | 0 1 0 0 |
; | 0 0 1 0 |
; | 0 0 0 1 |
; -         -
(defn identity-matrix []
  [ 1 0 0 0
    0 1 0 0
    0 0 1 0
    0 0 0 1])

; translate
; -         -
; | 1 0 0 tx |
; | 0 1 0 ty |
; | 0 0 1 tz |
; | 0 0 0 1  |
; -         -
(defn translate
  ([t]
    [ 1         0         0         0
      0         1         0         0
      0         0         1         0
      (nth t 0) (nth t 1) (nth t 2) 1 ])
  ([tx ty tz]
    [ 1  0  0  0
      0  1  0  0
      0  0  1  0
      tx ty tz 1 ]))

; scale
; -         -
; | x 0 0 0 |
; | 0 y 0 0 |
; | 0 0 z 0 |
; | 0 0 0 1 |
; -         -
(defn scale
  ([s]
    [ s  0  0  0
      0  s  0  0
      0  0  s  0
      0  0  0  1 ])
  ([sx sy sz]
    [ sx 0  0  0
      0  sy 0  0
      0  0  sz 0
      0  0  0  1 ]))

; rotation x
; -                       -
; | 1 0       0        0 |
; | 0 (cos a) (-sin a) 0 |
; | 0 (sin a) (cos a)  0 |
; | 0 0       0        1 |
; -                       -
(defn rotate-x [a]
  [ 1 0                0            0
    0 (Math/cos a)     (Math/sin a) 0
    0 (- (Math/sin a)) (Math/cos a) 0
    0 0                0            1 ])

; rotation y
; -                       -
; | (cos a)     0 (sin a) 0 |
; | 0           1 0       0 |
; | (- (sin a)) 0 (cos a) 0 |
; | 0           0 0       1 |
; -                       -
(defn rotate-y [a]
  [ (Math/cos a) 0 (- (Math/sin a)) 0
    0            1 0                0
    (Math/sin a) 0 (Math/cos a)     0
    0            0 0                1 ])

; rotation z
; -                       -
; | (cos a) (- sin a) 0 0 |
; | (sin a) (cos a)   0 0 |
; | 0       0         1 0 |
; | 0       0         0 1 |
; -                       -
(defn rotate-z [a]
  [ (Math/cos a)     (Math/sin a) 0 0
    (- (Math/sin a)) (Math/cos a) 0 0
    0                0            1 0
    0                0            0 1 ])

; matrix matrix multiplication
(defn mmul [a b]
  [
; column 0
  (+ (* (nth a 0) (nth b  0)) (* (nth a 4) (nth b  1)) (* (nth a  8) (nth b  2)) (* (nth a 12) (nth b  3)) )
  (+ (* (nth a 1) (nth b  0)) (* (nth a 5) (nth b  1)) (* (nth a  9) (nth b  2)) (* (nth a 13) (nth b  3)) )
  (+ (* (nth a 2) (nth b  0)) (* (nth a 6) (nth b  1)) (* (nth a 10) (nth b  2)) (* (nth a 14) (nth b  3)) )
  (+ (* (nth a 3) (nth b  0)) (* (nth a 7) (nth b  1)) (* (nth a 11) (nth b  2)) (* (nth a 15) (nth b  3)) )
; column 1
  (+ (* (nth a 0) (nth b  4)) (* (nth a 4) (nth b  5)) (* (nth a  8) (nth b  6)) (* (nth a 12) (nth b  7)) )
  (+ (* (nth a 1) (nth b  4)) (* (nth a 5) (nth b  5)) (* (nth a  9) (nth b  6)) (* (nth a 13) (nth b  7)) )
  (+ (* (nth a 2) (nth b  4)) (* (nth a 6) (nth b  5)) (* (nth a 10) (nth b  6)) (* (nth a 14) (nth b  7)) )
  (+ (* (nth a 3) (nth b  4)) (* (nth a 7) (nth b  5)) (* (nth a 11) (nth b  6)) (* (nth a 15) (nth b  7)) )
; column 2
  (+ (* (nth a 0) (nth b  8)) (* (nth a 4) (nth b  9)) (* (nth a  8) (nth b 10)) (* (nth a 12) (nth b 11)) )
  (+ (* (nth a 1) (nth b  8)) (* (nth a 5) (nth b  9)) (* (nth a  9) (nth b 10)) (* (nth a 13) (nth b 11)) )
  (+ (* (nth a 2) (nth b  8)) (* (nth a 6) (nth b  9)) (* (nth a 10) (nth b 10)) (* (nth a 14) (nth b 11)) )
  (+ (* (nth a 3) (nth b  8)) (* (nth a 7) (nth b  9)) (* (nth a 11) (nth b 10)) (* (nth a 15) (nth b 11)) )
; column 3
  (+ (* (nth a 0) (nth b 12)) (* (nth a 4) (nth b 13)) (* (nth a  8) (nth b 14)) (* (nth a 12) (nth b 15)) )
  (+ (* (nth a 1) (nth b 12)) (* (nth a 5) (nth b 13)) (* (nth a  9) (nth b 14)) (* (nth a 13) (nth b 15)) )
  (+ (* (nth a 2) (nth b 12)) (* (nth a 6) (nth b 13)) (* (nth a 10) (nth b 14)) (* (nth a 14) (nth b 15)) )
  (+ (* (nth a 3) (nth b 12)) (* (nth a 7) (nth b 13)) (* (nth a 11) (nth b 14)) (* (nth a 15) (nth b 15)) )
  ])

; matrix vector multiplication
(defn vmul [a b]
  [
; column 0
  (+ (* (nth a 0) (nth b  0)) (* (nth a 4) (nth b  1)) (* (nth a  8) (nth b  2)) (* (nth a 12) (nth b  3)) )
  (+ (* (nth a 1) (nth b  0)) (* (nth a 5) (nth b  1)) (* (nth a  9) (nth b  2)) (* (nth a 13) (nth b  3)) )
  (+ (* (nth a 2) (nth b  0)) (* (nth a 6) (nth b  1)) (* (nth a 10) (nth b  2)) (* (nth a 14) (nth b  3)) )
  (+ (* (nth a 3) (nth b  0)) (* (nth a 7) (nth b  1)) (* (nth a 11) (nth b  2)) (* (nth a 15) (nth b  3)) )
  ])

(defn get-translation [m]
  [(nth m 12) (nth m 13) (nth m 14)])

;https://stackoverflow.com/questions/15022630/how-to-calculate-the-angle-from-roational-matrix
;http://www.soi.city.ac.uk/~sbbh653/publications/euler.pdf
(defn get-rotation-x [m]
  (- (Math/atan2 (nth m  9)    ;r32
                 (nth m 10))))  ;r33

(defn get-rotation-y [m]
  (let [r31 (nth m  8)
        r32 (nth m  9)
        r33 (nth m 10)]
  (- (Math/atan2 (- r31)                         ;r31
                 (Math/sqrt (+ (* r32 r32)       ;r32
                               (* r33 r33))))))) ;r33

(defn get-rotation-z [m]
  (- (Math/atan2 (nth m  4)     ;r21
                 (nth m  0))))  ;r11

(defmacro coord [] `[(nth ~'coord-location 12) (nth ~'coord-location 13) (nth ~'coord-location 14)])
(defmacro coord-x [] `(nth ~'coord-location 12))
(defmacro coord-y [] `(nth ~'coord-location 13))
(defmacro coord-z [] `(nth ~'coord-location 14))

(defmacro scale-x [] `(nth ~'shape-scale  0))
(defmacro scale-y [] `(nth ~'shape-scale  5))
(defmacro scale-z [] `(nth ~'shape-scale 10))

(defn pretty [m]
  (cond
    (= 3 (count m)) (do
                      (print "v3 ")
                      (println (nth m 0) (nth m 1) (nth m 2)))
    (= 4 (count m)) (do
                      (print "v4 ")
                      (println (nth m 0) (nth m 1) (nth m 2) (nth m 3)))
    (= 16 (count m)) (do
                      (println "m4x4")
                      (println (nth m 0) (nth m 4) (nth m  8) (nth m 12) )
                      (println (nth m 1) (nth m 5) (nth m  9) (nth m 13) )
                      (println (nth m 2) (nth m 6) (nth m 10) (nth m 14) )
                      (println (nth m 3) (nth m 7) (nth m 11) (nth m 15) ))
    :else (println "3, 4 or 16 elements expected; got " (count m))))


(defn sample [m]
  (let [tr (get-translation m)]
    (println)
    (pretty m)
    (pretty tr)
    (pretty (vmul m [0 0 0 1]))))


; ----- color
(def cBlack   [0 0 0])
(def cRed     [0 1 1])
(def cYellow  [mPi3 1 1])
(def cGreen   [(* 2 mPi3) 1 1])
(def cCyan    [mPi 1 1])
(def cBlue    [(+ mPi mPi3) 1 1])
(def cMagneta [(+ mPi (* 2 mPi3)) 1 1])
(def cWhite   [0 0 1])
(def cGray    [0 0 0.5])
(def cOrange  [mPi8 1 1])

(defn hsv
  ([h s v] [h s v])
  ([h s v c3] [h s v]))

(defn extract-hue [m M c r g b]
  (let [rc   (/ (- M r) c)
        gc   (/ (- M g) c)
        bc   (/ (- M b) c)]
    (cond
      (= r M) (/ (- bc gc) 6)
      (= g M) (/ (+ 2 (- rc bc)) 6)
      :else   (/ (+ 4 (- gc rc)) 6))))

(defn rgb [r g b]
  (let [M (max r g b)
        m (min r g b)
        c (- M m)
        s (if (== M 0) 0 (/ c M))
        h (if (== c 0) 0 (extract-hue m M c r g b))]
    [(normalize2pi (* m2Pi h)) s M]))

(defn rgb-export
  "Convert HSV to RGB. see: https://de.wikipedia.org/wiki/HSV-Farbraum#Umrechnung_HSV_in_RGB"
   [hsv-vector]
  (let [h  (normalize2pi (nth hsv-vector 0))
        s  (nth hsv-vector 1)
        v  (nth hsv-vector 2)
        hi (Math/abs (int (/ h mPi3)))
        f  (- (/ h mPi3) hi)
        p  (* v (- 1 s))
        q  (* v (- 1 (* s f)))
        t  (* v (- 1 (* s (- 1 f))))]
    (cond
      (= 1 hi) [q v p]
      (= 2 hi) [p v t]
      (= 3 hi) [p q v]
      (= 4 hi) [t p v]
      (= 5 hi) [v p q]
      :else    [v t p])))

(defn h+
  "Add delta to the hue of color c3"
   [delta c3]
  [(normalize2pi (+ (nth c3 0) delta))
   (nth c3 1)
   (nth c3 2)])

(defn s-
  "Reduce saturation of color c3 by delta"
   [delta c3]
  [(nth c3 0)
   (limit (- (nth c3 1) delta))
   (nth c3 2)])

(defn v+
  "Increase value of color c3 by delta"
   [delta c3]
  [(nth c3 0)
   (nth c3 1)
   (limit (+ (nth c3 2) delta))])

; ----- pseudo random numbers
(def rg (java.util.Random. 42))
(defn rnd-seed [s] (.setSeed rg s))
(defn rnd-boolean   []       (.nextBoolean rg))
(defn rnd-discrete1 [rmax]   (inc (mod (.nextInt rg) rmax)))
(defn rnd-discrete  [rmax]   (mod (.nextInt rg) rmax))
(defn rnd-uniform   [rscale] (* (.nextDouble rg) rscale))
(defn rnd-gaussian  [rscale] (* (.nextGaussian rg) rscale))

(def perlin (PerlinNoise. 42))

(defn noise [xyz cscale rscale]
  (* rscale (.noise perlin (* cscale (nth xyz 0))
                           (* cscale (nth xyz 1))
                           (* cscale (nth xyz 2)))))

(defn v3-noise [xyz cscale rscale]
  ; see https://svn.blender.org/svnroot/bf-blender/trunk/blender/source/blender/python/mathutils/mathutils_noise.c
  [
    (* rscale (.noise perlin (* cscale (+ (nth xyz 0) 9.321))
                             (* cscale (- (nth xyz 1) 1.531))
                             (* cscale (- (nth xyz 2) 7.951))))
    (* rscale (.noise perlin (* cscale (nth xyz 0))
                             (* cscale (nth xyz 1))
                             (* cscale (nth xyz 2))))
    (* rscale (.noise perlin (* cscale (+ (nth xyz 0) 6.327 ))
                             (* cscale (+ (nth xyz 1) 0.1671))
                             (* cscale (- (nth xyz 2) 2.672))))
  ])

(declare turbulence)
(declare factor-turbulence)
(declare offset-turbulence)

(defn raw-turbulence [xyz cscale rscale octaves ampscale freqscale]
  (* (.turbulence perlin
             (* cscale (nth xyz 0)) (* cscale (nth xyz 1)) (* cscale (nth xyz 2))
             octaves ampscale freqscale)
     rscale))

(defn turbulence [xyz cscale rscale octaves ampscale freqscale]
  (* (- (raw-turbulence xyz cscale rscale octaves ampscale freqscale) offset-turbulence) factor-turbulence))

(defn calibrate-turbulence-range [octaves ampscale freqscale]
  (loop [ii 103 min +10000 max -10000]
    (if (> ii -103)
      (do
        (let [v   (raw-turbulence [(/ ii 31) (/ ii 31)  (/ ii 31)] 1 1 octaves ampscale freqscale)
              u   (raw-turbulence [(* ii 1.1) (* ii 1.2)  (* ii 1.3)] 1 1 octaves ampscale freqscale)
              uvi (if (< v u) v u)
              uva (if (> v u) v u)]
        (recur (dec ii)
               (if (< uvi min) uvi min)
               (if (> uva max) uva max))))
      [min max])))

(def minmax-turbulence (calibrate-turbulence-range 5 0.995 0.995))
(def offset-turbulence (first minmax-turbulence))
(def factor-turbulence (/ 1(- (second minmax-turbulence) (first minmax-turbulence))))


; ----- file system
(defn homepath [p]
  (if (str/starts-with? p "~") (str/replace-first p "~" (System/getenv "HOME")) p))

; ----- generators
(defn line [u x y z]
  (let [u2 (dec (max 2 u))
        dx (/ x u2)
        dy (/ y u2)
        dz (/ z u2)]
  (for [i (range (max 2 u))]
     (vector [(list 'translate (* i dx) (* i dy) (* i dz))]
             nil
             nil
             nil))))

(defn grid [u v w s]
  (let [u2 (/ (dec u) 2)
        v2 (/ (dec v) 2)
        w2 (/ (dec w) 2)]
  (for [i (range u) j (range v) k (range w)]
     (vector [(list 'translate (* s (- i u2)) (* s (- j v2)) (* s (- k w2)))]
             nil
             nil
             nil))))

(defn sphere-random [n r distribute]
  (defn rand-azimuth []
    (* m2Pi (.nextDouble rg)))
  (defn rand-polar []
    (* distribute m2Pi (.nextGaussian rg)))
  (for [i (range n)]
    (let [theta (rand-polar)
          phi   (rand-azimuth)]
;;       (println (degree phi) (degree theta) (type phi) (type theta))
      (vector
            ; :coord-location
            [(list 'rotate-z phi) (list 'rotate-y theta) (list 'translate r 0 0)]
            ; :shape-scale
            nil
            ; :shape-rotate
            [(list 'rotate-z phi) (list 'rotate-y theta)]
            ; :shape-color
             nil)
      )))

; see: https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere
; https://math.stackexchange.com/questions/1358046/is-the-fibonacci-lattice-the-very-best-way-to-evenly-distribute-n-points-on-a-sp
(defn sphere-fibonacci [n r]
  (for [i (range n)]
    (let [offset    (/ 2.0 n)
          increment (* Math/PI (- 3.0 (Math/sqrt 5.0)))
          y   (+ (- (* i offset) 1) (/ offset 2))
          r0   (Math/sqrt (- 1 (* y y)))
          phi (* i increment)
          x   (* r0 (Math/cos phi))
          z   (* r0 (Math/sin phi))
          rot-y (Math/atan2 z (Math/sqrt (+ (* x x) (* y y))))
          rot-z (Math/atan2 y x)]
      (vector
            ; :coord-location
            [(list 'rotate-z rot-z) (list 'rotate-y rot-y) (list 'translate r 0 0)]
            ; :shape-scale
            nil
            ; :shape-rotate
            [(list 'rotate-z rot-z) (list 'rotate-y rot-y)]
            ; :shape-color
            nil)
      )))

(defn spiral [n r delta-phi delta-z]
  (for [i (range n)]
    (let [phi (* i delta-phi)
          z   (* i delta-z)]
      (vector
            ; :coord-location
            [(list 'rotate-z phi) (list 'translate (* r i) 0 z)]
            ; :shape-scale
            nil
            ; :shape-rotate
            [(list 'rotate-z phi)]
            ; :shape-color
            nil)
      )))

(defn attractor [n0 n1 dt skip coord-func hsv-func x0 y0 z0]
  (loop [accu []
         i    0
         x    x0
         y    y0
         z    z0]
    (let [xyz (coord-func x y z)
          hsv (if (nil? hsv-func) nil (hsv-func x y z i))
          xn (+ x (* dt (nth xyz 0)))
          yn (+ y (* dt (nth xyz 1)))
          zn (+ z (* dt (nth xyz 2)))]
     (cond
       (< i n0) (recur accu (inc i) xn yn zn)
       (and (> skip 1) (< i n1) (pos? (mod i skip))) (recur accu (inc i) xn yn zn)
       (< i n1) (recur (conj accu (vector
                                    ; :coord-location
                                    [(list 'translate (- xn x) (- yn y) (- zn z))]
                                    nil
                                    nil
                                    ; :shape-color ; can be used as vertex color
                                    hsv))
                       (inc i)
                       xn yn zn)
       :else    accu))))

(defn chaotic-map [n0 n1 func x0 y0 z0]
  (loop [accu []
         i    0
         x    x0
         y    y0
         z    z0]
    (let [xyz (func x y z)
          xn (nth xyz 0)
          yn (nth xyz 1)
          zn (nth xyz 2)]
     (cond
       (< i n0) (recur accu (inc i) xn yn zn)
       (< i n1) (recur (conj accu (vector
                                    ; :coord-location
                                    [(list 'translate (- xn x) (- yn y) (- zn z))]
                                    nil
                                    nil
                                    nil))
                       (inc i)
                       xn yn zn)
       :else    accu))))

(defn translate-fn [n func]
  (loop [accu []
         i    0]
    (let [xyz (func i)
          xn (nth xyz 0)
          yn (nth xyz 1)
          zn (nth xyz 2)]
     (cond
       (< i n) (recur (conj accu (vector
                                    ; :coord-location
                                    [(list 'translate xn yn zn)]
                                    nil
                                    nil
                                    nil))
                       (inc i))
       :else    accu))))

(defn so-many [u]
  (for [i (range u)]
     (vector nil
             nil
             nil
             nil)))

; ----- imports as generator
(defn import-wavefront-rt [zoom path]
  (defn scanvec [line]
    (if (.startsWith line "v ")
      (let [parts (rest (str/split line #" "))]
        [(read-string (nth parts 0))
         (read-string (nth parts 1))
         (read-string (nth parts 2)) ])
      nil))

  (defn trans-rotate [v3 zoom]
    (let [loc-x (* zoom (nth v3 0))
          loc-y (* zoom (nth v3 1))
          loc-z (* zoom (nth v3 2))
          rot-y (Math/atan2 loc-z (Math/sqrt (+ (* loc-x loc-x) (* loc-y loc-y))))
          rot-z (Math/atan2 loc-y loc-x)]
    (vector
            ; :coord-location
            [
             (list 'rotate-z rot-z) (list 'rotate-y rot-y) (list 'translate zoom 0 0)
            ]
            ; :shape-scale
            nil
            ; :shape-rotate
            [(list 'rotate-z rot-z) (list 'rotate-y rot-y)]
            ; :shape-color
            nil)))

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (let [v3 (scanvec (first lines))]
          (if (nil? v3)
            (recur accu
                   (rest lines))
            (recur (conj accu (trans-rotate v3 zoom))
                   (rest lines))))
      ))))
(def import-wavefront (memoize import-wavefront-rt))

(defn import-vertices-rt [zoom path]
  (defn scanvec [line]
    (if (.startsWith line "v ")
      (let [parts (rest (str/split line #" "))]
        [(read-string (nth parts 0))
         (read-string (nth parts 1))
         (read-string (nth parts 2)) ])
      nil))

  (defn trans-only [v3 zoom]
    (let [loc-x (* zoom (nth v3 0))
          loc-y (* zoom (nth v3 1))
          loc-z (* zoom (nth v3 2))]
    (vector
            ; :coord-location
            [
             (list 'translate loc-x loc-y loc-z)
            ]
            ; :shape-scale
            nil
            ; :shape-rotate
            nil
            ; :shape-color
            nil)))

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (let [v3 (scanvec (first lines))]
          (if (nil? v3)
            (recur accu
                   (rest lines))
            (recur (conj accu (trans-only v3 zoom))
                   (rest lines))))
      ))))
(def import-vertices (memoize import-vertices-rt))

(defn import-neighbour-rt [zoom path]
  (defn scanvec3 [line offset]
      (let [parts (str/split line #" ")]
        [(read-string (nth parts (+ offset 0)))
         (read-string (nth parts (+ offset 1)))
         (read-string (nth parts (+ offset 2))) ]))

  (defn trans-rotate-normal [v3 n3 zoom]
    (let [loc-x (* zoom (nth v3 0))
          loc-y (* zoom (nth v3 1))
          loc-z (* zoom (nth v3 2))
          nor-x (nth n3 0)
          nor-y (nth n3 1)
          nor-z (nth n3 2)
          rot-y (+ mPi mPi2 (Math/atan2 (Math/sqrt (+ (* nor-x nor-x) (* nor-y nor-y))) nor-z))
          rot-z (Math/atan2 nor-y nor-x)]
    (vector
            ; :coord-location
            [
             (list 'translate loc-x loc-y loc-z) (list 'rotate-z rot-z) (list 'rotate-y rot-y)
            ]
            ; :shape-scale
            nil
            ; :shape-rotate
            [(list 'rotate-z rot-z) (list 'rotate-y rot-y)]
            ; :shape-color
            nil)))

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (let [v3 (scanvec3 (first lines) 1)
              n3 (scanvec3 (first lines) 4)]
           (recur (conj accu (trans-rotate-normal v3 n3 zoom))
                  (rest lines))))
      )))
(def import-neighbour (memoize import-neighbour-rt))


(defn import-bitmap-rt
  ([path] (import-bitmap-rt 1 1 1 path))
  ([scl path] (import-bitmap-rt scl scl scl path))
  ([scl-x scl-y scl-z path]
  (defn rgbv3 [rgb-pixel]
    (let [c (Color. rgb-pixel)]
      (rgb (/ (.getRed c) 255.0) (/ (.getGreen c) 255.0) (/ (.getBlue c) 255.0))))
  (println "<-" path)
  (let [abspath (homepath path)
        img     (ImageIO/read (io/as-file abspath))
        width   (.getWidth  img)
        height  (.getHeight img)]
    (for [w (range width) h (range height)]
      (vector [(list 'translate (* w scl-x) (* h scl-y) 0)]
              [(list 'scale scl-x scl-y scl-z)]
              nil
              (rgbv3 (.getRGB img w h))))
    )))
(def import-bitmap (memoize import-bitmap-rt))


(defn import-csv-rt [zoom path]
  (defn scanvec6 [csvline]
    (let [parts (str/split csvline #",")]
      [(read-string (nth parts 0))
       (read-string (nth parts 1))
       (read-string (nth parts 2))
       (read-string (nth parts 3))
       (read-string (nth parts 4))
       (read-string (nth parts 5)) ]))

  (defn trans-scale [v6 zoom]
    (let [loc-x (* zoom (nth v6 0))
          loc-y (* zoom (nth v6 1))
          loc-z (* zoom (nth v6 2))
          scl-x (* zoom (nth v6 3))
          scl-y (* zoom (nth v6 4))
          scl-z (* zoom (nth v6 5)) ]
    (vector
            ; :coord-location
            [(list 'translate loc-x loc-y loc-z)]
            ; :shape-scale
            [(list 'scale scl-x scl-y scl-z)]
            ; :shape-rotate
            nil
            ; :shape-color
            nil)))

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (let [v6 (scanvec6 (first lines))]
          (recur (conj accu (trans-scale v6 zoom))
                 (rest lines)))
      ))))
(def import-csv (memoize import-csv-rt))


(defn import-ana-vert-rt [zoom path]
  (defn trans-scale-rotate [csvline zoom]
    (let [parts (str/split csvline #",")
          loc-x (* zoom (read-string (nth parts 3)))
          loc-y (* zoom (read-string (nth parts 4)))
          loc-z (* zoom (read-string (nth parts 5)))
          scl-x (* zoom (read-string (nth parts 6)))
          scl-y (* zoom (read-string (nth parts 7)))
          scl-z (* zoom (read-string (nth parts 8)))
          rot-z (read-string (nth parts 9))
          rot-y (read-string (nth parts 10))
          red   (read-string (nth parts 11))
          green (read-string (nth parts 12))
          blue  (read-string (nth parts 13))
           ]
    (vector
            ; :coord-location
            [(list 'translate loc-x loc-y loc-z)]
            ; :shape-scale
            [(list 'scale scl-x scl-y scl-z)]
            ; :shape-rotate
            [(list 'rotate-z rot-z) (list 'rotate-y rot-y)]
            ; :shape-color
            (rgb red green blue))))

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (loop [accu []
           lines (line-seq rdr)]
      (if (empty? lines)
        accu
        (recur (conj accu (trans-scale-rotate (first lines) zoom))
               (rest lines))))))
(def import-ana-vert (memoize import-ana-vert-rt))


; ----- data imports
(defrecord DataField [width height depth a])

(defn csv-data
  "data imports from csv files with three colums"
  [path]
  (let [scanvec3 (fn [line ^doubles ary i]
                   (let [lparts (str/split line #",")]
                     (aset ary i       (Double. ^String (nth lparts 0)))
                     (aset ary (+ i 1) (Double. ^String (nth lparts 1)))
                     (aset ary (+ i 2) (Double. ^String (nth lparts 2)))
                     ary))]

  (with-open [rdr (io/reader (homepath path))]
    (println "<-" path)
    (let [header (first (line-seq rdr))
          hparts (str/split header #",")
          w (Integer. ^String (nth hparts 0))
          h (Integer. ^String (nth hparts 1))
          d (Integer. ^String (nth hparts 2))
          ary (make-array Double/TYPE (* w h d))]
      (loop [accu ary
             i 0
             lines (rest (line-seq rdr))]
        (if (empty? lines)
          accu
          (recur (scanvec3 (first lines) ary i) (+ i 3) (rest lines))
        ))
      (DataField. w h d ary)))))

(defn value-at
  "Read single value from data field."
  [a xp yp dp inrange]
  (let [w   (:width  a)
        h   (:height a)
        d   (:depth  a)
        ix  (int (maprange inrange [0 w] xp))
        iy  (int (maprange inrange [0 h] yp))
        ii  (+ dp
               (* iy w d)
               (* ix d))
        iic (clamp ii 0 (dec (* w h d)))]
    (aget ^doubles (:a a) iic)))


; -----
(defn data
  "data imports from bitmap image files"
  [path]
  (let [abspath (homepath path)
        img    (ImageIO/read (io/as-file abspath))
        width  (.getWidth  img)
        height (.getHeight img)]
    (println "<-" path)
    [width height img]))

(defn data-size [d]
  [(nth d 0) (nth d 1)])

(defn data-size-x [d]
  (nth d 0))

(defn data-size-y [d]
  (nth d 1))

(defn at [d x y]
  (let [wx (* x (nth d 0))
        hy (* y (nth d 1))]
        (cond
          (neg? wx)         0
          (>= wx (nth d 0)) 0
          (neg? hy)         0
          (>= hy (nth d 1)) 0
          :else
          (let [raw-rgb (.getRGB (nth d 2) wx hy)
                c   (Color. raw-rgb)]
            (/ (+ (.getRed c) (.getGreen c) (.getBlue c)) 255.0 3.0)))))

(defn red-at [d x y]
  (let [wx (* x (nth d 0))
        hy (* y (nth d 1))]
        (cond
          (neg? wx)         0
          (>= wx (nth d 0)) 0
          (neg? hy)         0
          (>= hy (nth d 1)) 0
          :else
          (let [raw-rgb (.getRGB (nth d 2) wx hy)
                c   (Color. raw-rgb)]
            (/ (.getRed c) 255.0)))))

(defn green-at [d x y]
  (let [wx (* x (nth d 0))
        hy (* y (nth d 1))]
        (cond
          (neg? wx)         0
          (>= wx (nth d 0)) 0
          (neg? hy)         0
          (>= hy (nth d 1)) 0
          :else
          (let [raw-rgb (.getRGB (nth d 2) wx hy)
                c   (Color. raw-rgb)]
            (/ (.getGreen c) 255.0)))))

(defn blue-at [d x y]
  (let [wx (* x (nth d 0))
        hy (* y (nth d 1))]
        (cond
          (neg? wx)         0
          (>= wx (nth d 0)) 0
          (neg? hy)         0
          (>= hy (nth d 1)) 0
          :else
          (let [raw-rgb (.getRGB (nth d 2) wx hy)
                c   (Color. raw-rgb)]
            (/ (.getBlue c) 255.0)))))

(defn hsv-at [d x y]
  (let [wx (* x (nth d 0))
        hy (* y (nth d 1))]
        (cond
          (neg? wx)         cRed
          (>= wx (nth d 0)) cRed
          (neg? hy)         cRed
          (>= hy (nth d 1)) cRed
          :else
          (let [raw-rgb (.getRGB (nth d 2) wx hy)
                c   (Color. raw-rgb)
                r   (/ (.getRed c) 255.0)
                g   (/ (.getGreen c) 255.0)
                b   (/ (.getBlue c) 255.0)]
          (rgb r g b)))))


; ----- stringtemplate
(def counter (atom 0N))
(def initial-color cWhite)

(declare st-container)
(declare st-group)
(declare stg-path)

(defn format-attrib-map [attr]
  (map #(list (first %) (second %)) attr))

(defn add-shape [templ attr depth
                 location
                 shape-scale-x  shape-scale-y  shape-scale-z
                 shape-rotate-x shape-rotate-y shape-rotate-z
                 color]
  (if (and (not (empty? templ)) (pos? shape-scale-x) (pos? shape-scale-y) (pos? shape-scale-z))
    (let [n    (swap! counter inc)
          tmpl (first templ)
          pref (second templ)
          st   (.getInstanceOf st-group (str tmpl))]
      (if (nil? st) (throw (Exception. (str "Missing template '" (str tmpl) "' in '" (str stg-path) "'"))))
      (.add st "seq" n)
      (.add st "depth" depth)
      (if (some? pref) (.add st "pref"  pref))
      (.add st "attr" (format-attrib-map attr))
      (.add st "x"  (double (nth location 0)))
      (.add st "y"  (double (nth location 1)))
      (.add st "z"  (double (nth location 2)))
      (.add st "sx" (double shape-scale-x))
      (.add st "sy" (double shape-scale-y))
      (.add st "sz" (double shape-scale-z))
      (.add st "rx" (double shape-rotate-x))
      (.add st "ry" (double shape-rotate-y))
      (.add st "rz" (double shape-rotate-z))
      (if (vector? color)
        (let [rgb (rgb-export color)]
          (.add st "r"  (double (nth rgb 0)))
          (.add st "g"  (double (nth rgb 1)))
          (.add st "b"  (double (nth rgb 2))))
        (.add st "ref" color))
      st)))

; ----- stringtemplate for Blender
(defn bpyversion [n_me]
  (let [bvexists   (System/getenv "BPYVERSION")
        bpyversion (if bvexists bvexists "28")
        n_ma        (if (nil? bvexists) "1" (if (= bpyversion "27") "N" "1"))]
        (println bvexists bpyversion n_me n_ma)
        (str "resources/clj_bpy" bpyversion "_me" n_me "_ma" n_ma ".stg")))

(defn template-bpy [& n_me]
  (def stg-path (bpyversion (if n_me (first n_me) "N")))
  (def out-extension ".py")
  (def st-group
    (do (println "<-" stg-path)
      (STGroupFile. stg-path))))

; ----- stringtemplate for HP-GL/2
(defn template-hpgl []
  (def stg-path "resources/clj_hpgl.stg")
  (def out-extension ".line")
  (def st-group
    (do (println "<-" stg-path)
      (STGroupFile. stg-path))))

; ----- stringtemplate for OpenVDB
(defn template-vdb []
  (def stg-path "resources/clj_vdb_vl.stg")
  (def out-extension ".vl")
  (def st-group
    (do (println "<-" stg-path)
      (STGroupFile. stg-path))))

; ----- stringtemplate for SVG
(def bounds-x (transient []))
(def bounds-y (transient []))
(defn template-svg [& post-zoom]
  (def asvg-zoom (atom (if post-zoom (first post-zoom) 1)))

  (def stg-path "resources/clj_svg.stg")
  (def out-extension ".svg")
  (def st-group
    (do (println "<-" stg-path)
      (STGroupFile. stg-path)))

  (def path-index (atom 0N))

  (defn format-color [c] (int (* 100 c)))
  (defn format-rot   [r] (double (/ (* 180 r) mPi)))
  (defn output-location [x y z sx sy sz st]
    (conj! bounds-x (+ x sx)) (conj! bounds-x (- x sx))
    (conj! bounds-y (+ y sy)) (conj! bounds-y (- y sy))
    (.add st "x" x)
    (.add st "y" y)
    (.add st "z" z)
    (.add st "sx" sx)
    (.add st "sy" sy)
    (.add st "sz" sz))
  (defn render-location [x y z sx sy sz t st]
    (let [svg-zoom @asvg-zoom
          pi (if (or (= t "curve_point") (= t "line_point"))
                 (swap! path-index inc)
                 (reset! path-index 0))]
      (.add st "isfirst" (= pi 1))
      (cond
        (= t "cube")
        (output-location
          (* svg-zoom (- x (* 0.5 sx)))
          (* svg-zoom (- y (* 0.5 sy)))
          z
          (* svg-zoom sx)
          (* svg-zoom sy)
          sz
          st)
        (= t "sphere")
        (output-location
          (* svg-zoom x)
          (* svg-zoom y)
          z
          (* svg-zoom (* 0.5 sx))
          (* svg-zoom (* 0.5 sy))
          sz
          st)
        :else
        (output-location
          (* svg-zoom x)
          (* svg-zoom y)
          z
          (* svg-zoom sx)
          (* svg-zoom sy)
          sz
          st)
        )))

  ; overwrite initial color.
  (def initial-color cBlack)

  ; overwrite the default add-shape function. SVG needs some extras.
  (defn add-shape [templ attr depth
                  location
                  shape-scale-x  shape-scale-y  shape-scale-z
                  shape-rotate-x shape-rotate-y shape-rotate-z
                  color]
    (if (and (not (empty? templ)) (pos? shape-scale-x) (pos? shape-scale-y) (pos? shape-scale-z))
      (let [n    (swap! counter inc)
            tmpl (first templ)
            pref (second templ)
            st   (.getInstanceOf st-group (str tmpl))]
        (if (nil? st) (throw (Exception. (str "Missing template '" (str tmpl) "' in '" (str stg-path) "'"))))
        (.add st "seq" n)
        (.add st "depth" depth)
        (if (some? pref) (.add st "pref"  pref))
        (.add st "attr" (format-attrib-map attr))
        (render-location (double (nth location 0))
                         (double (nth location 1))
                         (double (nth location 2))
                         (double shape-scale-x)
                         (double shape-scale-y)
                         (double shape-scale-z)
                         tmpl st)
        (.add st "rx" (format-rot shape-rotate-x))
        (.add st "ry" (format-rot shape-rotate-y))
        (.add st "rz" (format-rot shape-rotate-z))
        (if (and (vector? color) (> (nth color 2) 0))
          (let [rgb (rgb-export color)]
            (.add st "r"  (format-color (nth rgb 0)))
            (.add st "g"  (format-color (nth rgb 1)))
            (.add st "b"  (format-color (nth rgb 2))))
          (.add st "ref" color))
        st))))

; ----- stringtemplate for dumping raw values
(defn template-csv []
  (def stg-path "resources/clj_csv.stg")
  (def out-extension ".csv")
  (def st-group
    (do (println "<-" stg-path)
      (STGroupFile. stg-path))))

;; (defn add-trace [templ attr preceding location
;;                 shape-scale-x shape-scale-y shape-scale-z
;;                 color]
;;   (if (and (not (empty? templ)) (pos? shape-scale-x) (pos? shape-scale-y) (pos? shape-scale-z))
;;     (let [n   (swap! counter inc)
;;           rgb (rgb-export color)
;;           dx  (- (nth location 0) (nth preceding 0))
;;           dy  (- (nth location 1) (nth preceding 1))
;;           dz  (- (nth location 2) (nth preceding 2))
;;           len (Math/sqrt (+ (* dx dx) (* dy dy) (* dz dz)))
;;           mx  (/ (+ (nth location 0) (nth preceding 0)) 2)
;;           my  (/ (+ (nth location 1) (nth preceding 1)) 2)
;;           mz  (/ (+ (nth location 2) (nth preceding 2)) 2)
;;           st  (.getInstanceOf st-group (nth (cycle templ) n))
;;           fs  0.5]
;;       (if (nil? st) (throw (Exception. (str "Missing template '" (nth (cycle templ) n) "' in '" stg-path "'"))))
;;       (.add st "seq" n)
;;       (.add st "depth" 0)
;;       (.add st "attr" (format-attrib-map attr))
;;       (.add st "dx" dx)
;;       (.add st "dy" dy)
;;       (.add st "dz" dz)
;;       (.add st "x"  mx)
;;       (.add st "y"  my)
;;       (.add st "z"  mz)
;;       (.add st "sx" (double (* shape-scale-x fs)))
;;       (.add st "sy" (double (* shape-scale-y fs)))
;;       (.add st "sz" (double (* fs len)))
;;       (.add st "r"  (double (nth rgb 0)))
;;       (.add st "g"  (double (nth rgb 1)))
;;       (.add st "b"  (double (nth rgb 2)))
;;       st)))

(defmacro emit-to-shapes [templ attr depth coord-location shape-scale shape-rotate shape-color]
  `(.add st-container "shapes"
      (add-shape ~templ ~attr ~depth (vmul ~coord-location [0 0 0 1])
                 (nth ~shape-scale 0) (nth ~shape-scale 5) (nth ~shape-scale 10)
                 (get-rotation-x ~shape-rotate) (get-rotation-y ~shape-rotate) (get-rotation-z ~shape-rotate)
;;TODO           (get-rotation-x ~shape-scale) (get-rotation-y ~shape-scale) (get-rotation-z ~shape-scale)
                 ~shape-color)))

;; (defmacro emit-trace [templ preceding-location coord-location shape-scale shape-color]
;;   `(.add st-container "shapes"
;;       (add-trace ~templ {} ;TODO support attributes
;;                  (vmul ~preceding-location [0 0 0 1])
;;                  (vmul ~coord-location [0 0 0 1])
;;                  (nth ~shape-scale 0) (nth ~shape-scale 5) (nth ~shape-scale 10)
;;                  ~shape-color)))

(defn emit-object [template attr depth & args]
  (let [args-map (apply hash-map args)]
    (emit-to-shapes template attr depth
                (args-map :coord-location)
                (args-map :shape-scale)
                (args-map :shape-rotate)
                (args-map :shape-color))))

; ----- expand grow dsl
(def export-path "/dev/shm/grow-export/")
(shell/sh "mkdir" export-path)

(defmacro build-rname [n i]
  `(symbol (str ~n "_" ~i)))

(defmacro declare-alternate-rnames [fq-map aname]
  `(reverse (into (list 'declare)
                  (for [i# (range (inc (~fq-map ~aname)))]
                       (symbol (str ~aname (if (pos? i#) (str "_" i#))))))))

(defn unroll-keyword-args []
  { :keys (vector 'coord-location 'shape-scale 'shape-rotate 'shape-color)
      :or   { 'coord-location (identity-matrix)
              'shape-scale    (identity-matrix)
              'shape-rotate   (identity-matrix)
              'shape-color    initial-color}})

(defn rule-call [rname freq args]
  (do
    (if (< freq 2)
     `(defn ~rname [~'depth ~'preceding-location ~@args & ~(unroll-keyword-args)]
                        (~(build-rname rname 1)
                              ~'depth ~'preceding-location ~@args
                              ~'coord-location
                              ~'shape-scale
                              ~'shape-rotate
                              ~'shape-color))
     `(defn ~rname [~'depth ~'preceding-location ~@args & ~(unroll-keyword-args)]
                        ~(loop [fi 1
                                accu (reverse `(cond))]
                           (if (<= fi freq)
                             (recur (inc fi) (conj accu `(= ~'n ~fi)
                                                        `(~(build-rname rname fi) ~'depth ~'preceding-location ~@args
                                                                                  ~'coord-location
                                                                                  ~'shape-scale
                                                                                  ~'shape-rotate
                                                                                  ~'shape-color)))
                             `(let [~'n (rnd-discrete1 ~freq)] ~(reverse accu))))))))

(defmacro trait [t]
  (def global-traits t))

(defmacro grow [ & inner-nodes]
  (let [rns# (for [node inner-nodes
               :let [rname (second node)]
               :when (= (first node) 'rule)]
               rname)
        args-map# (into {} (for [node inner-nodes
                             :let [rarg (nth node 2 nil)]
                             :when (and (>= (count node) 3)
                                        (= (first node) 'rule)
                                        (vector? (nth node 2)))]
                             [(second node) rarg]))
        fq-map# (frequencies rns#)
        rnames# (keys fq-map#)]
;;     (println rns#)
;;     (println args-map#)
;;     (println fq-map#)
;;     (println rnames#)
    (println "<-" *file*)
    (reset! counter 0)
    (.setSeed rg 42)
    (def st-container (.getInstanceOf st-group "container"))
    (.unload st-group)
    (.add st-container "traits" (format-attrib-map global-traits))
    (eval (reverse (into '(do) (map #(list 'def (symbol (str % "_c")) 0) rnames#))))
    `(time (do
             ~@(map #(declare-alternate-rnames fq-map# %) rnames#)
             ~@(loop [rls `(~@rnames#)
                      rule-code '()]
                 (if (seq rls)
                   (recur (rest rls) (conj rule-code (rule-call (first rls)
                                                                (fq-map# (first rls))
                                                                (get args-map# (first rls) []))))
                   (reverse rule-code)
                 ))
             ~@inner-nodes
             (let [;;auto-name#     (str/join "." (reverse (str/split (str (first *command-line-args*)) #"\.")))
                   auto-name#     (str/join "." (str/split (first *command-line-args*) #"/"))
                   export-target# (str export-path auto-name# out-extension)
                   recent-link#   (str export-path "recentexport" out-extension)]
               (println "->" export-target#)
               (shell/sh "rm" export-target# recent-link#)
               (with-open [wrtr# (io/writer export-target#)]
                 (.write wrtr# (.render st-container)))
               (shell/sh "ln" "-T" "--symbolic" export-target# recent-link#))
             (.unload st-group)
             (println @counter)
             (if (pos? (count bounds-x))
               (let [bx# (persistent! bounds-x)
                     by# (persistent! bounds-y)
                     minx# (apply min bx#)
                     miny# (apply min by#)]
                     (println "viewBox=\"" minx# miny# (- (apply max bx#) minx#) (- (apply max by#) miny#) "\"")))
             (System/exit 0)))))

(defmacro rule [rname & rule-content]
  (intern *ns* (symbol (str rname "_c")) (inc (var-get (intern *ns* (symbol (str rname "_c"))))))
  (if (vector? (first rule-content))
    (let [rcall (first rule-content)
          body (rest rule-content)]
      `(defn ~(symbol (str rname "_" (var-get (intern *ns* (symbol (str rname "_c"))))))
         [~'depth ~'preceding-location ~@rcall ~'coord-location ~'shape-scale ~'shape-rotate ~'shape-color]
         (if (>= ~'depth 0)
           (do ~@body))))
    `(defn ~(symbol (str rname "_" (var-get (intern *ns* (symbol (str rname "_c"))))))
       [~'depth ~'preceding-location ~'coord-location ~'shape-scale ~'shape-rotate ~'shape-color]
       (if (>= ~'depth 0)
         (do ~@rule-content)))))

(defmacro start [rcall depth]
  (if (vector? rcall)
    `(~(first rcall) ~depth (identity-matrix) ~@(rest rcall))
    `(~rcall ~depth (identity-matrix))))

;; (defmacro trace [template]
;;   (if (not (empty? template))
;;    `(emit-trace ~template ~'preceding-location ~'coord-location ~'shape-scale ~'shape-color)))

(defn unroll-mmul [target transformations]
  `(reduce mmul (into [~target] ~transformations))
)

(defn missing-aspect?
  [seq elm]
  (not (some #(= elm %) seq)))

(defn dsl-keyword? [a] (or (= :coord a) (= :scale a) (= :rotate a) (= :color a)))

(defn expand-keyword-args [etc accu]
  (if (not (empty? etc))
    (cond
      (= :coord (first etc))
        (expand-keyword-args (rest (rest etc)) (conj accu :coord-location (unroll-mmul 'coord-location (second etc))))
      (= :scale (first etc))
        (expand-keyword-args (rest (rest etc)) (conj accu :shape-scale (unroll-mmul 'shape-scale (second etc))))
      (= :rotate (first etc))
        (expand-keyword-args (rest (rest etc)) (conj accu :shape-rotate (unroll-mmul 'shape-rotate (second etc))))
      (= :color (first etc))
        (do
          (expand-keyword-args (rest (rest etc)) (conj accu :shape-color (second etc)))
         )
      :else
        (expand-keyword-args (rest etc) (conj accu (first etc))))
    (reverse accu)))

(defn unroll-invoke [rn args]
  (let [keyword-args (drop-while #(not (dsl-keyword? %)) args)
        user-args    (take-while #(not (dsl-keyword? %)) args)]
    (cond
      (nil? keyword-args)
        (filter #(not (nil? %))
          (list rn (list 'dec 'depth) 'coord-location (first user-args) (second user-args) ;TODO (first user-args) (second user-args)
                :coord-location 'coord-location
                :shape-scale    'shape-scale
                :shape-rotate   'shape-rotate
                :shape-color    'shape-color))
      :else
        (do
          (expand-keyword-args keyword-args (reverse
                      (filter #(not (nil? %))
                      (list rn (list 'dec 'depth) 'coord-location (first user-args) (second user-args) ;TODO (first user-args) (second user-args)
                                      (if (missing-aspect? keyword-args :coord)  :coord-location)
                                      (if (missing-aspect? keyword-args :coord)  'coord-location)
                                      (if (missing-aspect? keyword-args :scale)  :shape-scale)
                                      (if (missing-aspect? keyword-args :scale)  'shape-scale)
                                      (if (missing-aspect? keyword-args :rotate) :shape-rotate)
                                      (if (missing-aspect? keyword-args :rotate) 'shape-rotate)
                                      (if (missing-aspect? keyword-args :color)  :shape-color)
                                      (if (missing-aspect? keyword-args :color)  'shape-color))
                                   )))))
  ))

(defmacro invoke [rn & args]
  (unroll-invoke rn args))

(defn unroll-object [template args]
  (let [attr0        (first (filter #(map? %) args))
        attr         (if (nil? attr0) {} attr0)
        no-attr      (filter #(not (map? %)) args)
        keyword-args (drop-while #(not (dsl-keyword? %)) no-attr)
        user-args    (take-while #(not (dsl-keyword? %)) no-attr)]
;;     (println "attr0       " attr0)
;;     (println "attr        " attr)
;;     (println "no-attr     " no-attr)
;;     (println "keyword-args" keyword-args)
;;     (println "user-args   " user-args)
    (cond
      (nil? keyword-args)
        (filter #(not (nil? %))
          (list 'emit-object template attr 'depth (first user-args) (second user-args) ;TODO (first user-args) (second user-args)
                :coord-location 'coord-location
                :shape-scale    'shape-scale
                :shape-rotate   'shape-rotate
                :shape-color    'shape-color))
      :else
        (do
          (expand-keyword-args keyword-args (reverse
                      (filter #(not (nil? %))
                      (list 'emit-object template attr 'depth (first user-args) (second user-args) ;TODO (first user-args) (second user-args)
                                      (if (missing-aspect? keyword-args :coord)  :coord-location)
                                      (if (missing-aspect? keyword-args :coord)  'coord-location)
                                      (if (missing-aspect? keyword-args :scale)  :shape-scale)
                                      (if (missing-aspect? keyword-args :scale)  'shape-scale)
                                      (if (missing-aspect? keyword-args :rotate) :shape-rotate)
                                      (if (missing-aspect? keyword-args :rotate) 'shape-rotate)
                                      (if (missing-aspect? keyword-args :color)  :shape-color)
                                      (if (missing-aspect? keyword-args :color)  'shape-color))
                                   )))))
  ))

(defmacro emit [template & args]
  (if (not (empty? template))
    (unroll-object template args)))

(defmacro generator [rules all-elements]
  (if (> (count rules) 1)
    `(loop [gen-list# ~all-elements
            ix# 0]
       (if (seq gen-list#)
         (let [gen-node# (first gen-list#)]
           ((nth (cycle ~rules) ix#)
             (dec ~'depth)
             ~'preceding-location
             :coord-location
               (if (seq (nth gen-node# 0))
                 (reduce mmul (into [~'coord-location] (eval (nth gen-node# 0))))
                 ~'coord-location)
             :shape-scale
               (if (seq (nth gen-node# 1))
                 (reduce mmul (into [~'shape-scale] (eval (nth gen-node# 1))))
                 ~'shape-scale)
             :shape-rotate
               (if (seq (nth gen-node# 2))
                 (reduce mmul (into [~'shape-rotate] (eval (nth gen-node# 2))))
                 ~'shape-rotate)
             :shape-color
               (if (seq (nth gen-node# 3))
                 (nth gen-node# 3)
                 ~'shape-color))
           (recur (rest gen-list#) (inc ix#)))))
    (let [single-rule (first rules)]
      `(loop [gen-list# ~all-elements]
         (if (seq gen-list#)
           (let [gen-node# (first gen-list#)]
             (~single-rule
               (dec ~'depth)
                ~'preceding-location
               :coord-location
                 (if (seq (nth gen-node# 0))
                   (reduce mmul (into [~'coord-location] (eval (nth gen-node# 0))))
                   ~'coord-location)
               :shape-scale
                 (if (seq (nth gen-node# 1))
                   (reduce mmul (into [~'shape-scale] (eval (nth gen-node# 1))))
                   ~'shape-scale)
               :shape-rotate
                 (if (seq (nth gen-node# 2))
                   (reduce mmul (into [~'shape-rotate] (eval (nth gen-node# 2))))
                   ~'shape-rotate)
               :shape-color
                 (if (seq (nth gen-node# 3))
                   (nth gen-node# 3)
                   ~'shape-color))
             (recur (rest gen-list#))))))))
