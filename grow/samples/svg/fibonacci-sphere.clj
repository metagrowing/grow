(use 'grow.core)
(template-svg)

(grow
  (rule C
    (emit ["sphere"] :scale [(scale (/ 200 (v3-len (v3-set-z 0 (coord)))))])
  )

  (rule G
    (generator [C] (sphere-fibonacci 400 100))
  )
  
  (start G 99)
)
