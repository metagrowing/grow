(use 'grow.core)
(template-svg)
(trait {:stroke-width 0.1 } )

(grow
  (rule T
    (emit ["cube"] :scale  [(scale 16)])
    (if (or (rnd-boolean) (rnd-boolean))
      (invoke T :scale  [(scale 0.8)]
                :rotate [(rotate-z (rnd-gaussian (noise (coord) 0.05 0.1)))]))
  )

  (rule G
    (generator [T] (grid 11 11 1 18))
  )
  
  (start G 23)
)
