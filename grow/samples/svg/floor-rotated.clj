(use 'grow.core)
(template-svg)
(trait {:stroke-width 0.4 } )

(grow
  (def step-size 4)
  (def step-angle 8)
  (def factor-angle (/ m2Pi step-angle))
  
  (defn n-floor [c] (* (Math/floor (noise c 0.0125 step-angle)) factor-angle))
  
  (rule C
    ;;(println (degree (n-floor (coord)))) ; debug print
    (emit ["cube"] :scale  [(scale (* 1 step-size)
                                   (* 0.1 step-size)
                                   (* 0.1 step-size))]
                   :rotate [(rotate-z (n-floor (coord)))])
  )

  (rule G
    (generator [C] (grid 48 42 1 step-size))
  )
  
  (start G 99)
)
