(use 'grow.core)
(template-svg)
(trait {:stroke-width 0.4 } )

(grow
  (def step-size 8)
  
  (rule C
    (emit ["cube"] :scale  [(scale (* 0.8 step-size))]
                   :rotate [(rotate-z (- (noise (coord) 0.0125 mPi) mPi2))])
  )

  (rule G
    (generator [C] (grid 29 21 1 step-size))
  )
  
  (start G 99)
)
