(use 'grow.core)
(template-svg)
(trait {:stroke-width 0.1 } )

(grow
  (def lines 125)
  (def segements 175)
  
  (rule P
    (emit ["line_point"])
  )

  (rule T
    (invoke P :coord [(translate 0 (noise (coord) 0.1 10) 0)])
  )

  (rule L
    (emit ["line_base"])
    (generator [T] (grid segements 1 1 1))
    (emit ["line_end"])
  )
  
  (rule G
    (generator [L] (grid 1 lines 1 1))
  )
  
  (start G 40)
)
