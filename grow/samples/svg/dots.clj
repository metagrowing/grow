(use 'grow.core)
(template-svg)
(trait {:stroke-width 0.1 } )

(grow
  (def noise-sample 0.05)
  (def step-size 16)
  (def noise-scale (* 0.9 step-size))
  (def noise-move (* -0.5 noise-scale))
  
  (rule S
    (emit ["sphere"] :coord [(translate (v3-move noise-move
                                                 (v3-noise (coord) noise-sample noise-scale)))]
                     :scale [(scale (* 0.35 step-size))]
                     :color cBlack)
    (invoke S :scale [(scale 0.7)])
  )

  (rule C
    (emit ["cube"] :scale [(scale (* 0.8 step-size))]
                   :color cBlack)
    (invoke S)
  )

  (rule G
    (generator [C] (grid 11 11 1 step-size))
  )
  
  (start G 6)
)
