(use 'grow.core) ; maps this grow DSL to Clojure
(template-bpy)   ; use a template suitable for Blenders Python API

(grow
  (def p 6)
  (def t (* 0.5 p))
  (def loops 24)

  ; Generates red spheres as petal.
  (rule Petal
    (emit ["sphere"] :scale [(scale 6 6 1)]
                     :color cRed)
  )
  
  ; Generates a pedicel. Use color cGreen from upstream.
  (rule Pedicel
    (emit ["cylinder"] :scale [(scale 0.2 0.2 t)])
    (invoke Petal
            :coord [(translate 0 0 p)])
  )
  
  ; Generates some pedicel. l is used like a 'for loop' counter.
  (rule Loop [l]
    (if (pos? l)
      (let [a (radian (* 2.5 (dec l)))]
        (invoke Loop (dec l)
                :coord  [(rotate-z mGA)]
                :rotate [(rotate-z mGA)])
        (invoke Pedicel
                :coord  [(rotate-y a) (translate 0 0 p)]
                :scale [(scale 1 1 t)]
                :rotate [(rotate-y a)])))
  )
  
  ; Start the Loop rule.
  (rule Trunc
    (emit ["cylinder"] :scale [(scale 0.5 0.5 p)])
    (invoke Loop loops)
  )
  
  (rule S
    (invoke Trunc
            :coord  [(translate 0 0 t)]
            :color  cGreen)
  )
  
  ; Expanding the rules start here.
  ; Rule Loop is recursively defined, but is limited by (def loops 24).
  (start S 999)
)
