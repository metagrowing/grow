(use 'grow.core) ; maps this grow DSL to Clojure
(template-bpy)   ; use a template suitable for Blenders Python API

(grow
  (def grid-dist 0.1)
  (def flat      (* 0.25 grid-dist))
  ; Some constants to control the amount of noise.
  (def coord-scale  3)
  (def result-scale 6)
  (def half-scale   (* result-scale 0.5))

  ; Generate a scaled cube.
  ; Rotated by Perlin noise calculated at the current position.
  (rule C
    (emit ["cube"] 
          :scale  [(scale grid-dist flat (* 2 grid-dist))]
          :rotate [(rotate-x (- (noise (coord) coord-scale result-scale) half-scale))])
  )

  ; Generate a two dimensional grid.
  (rule G
    (generator [C] (grid 20 20 1 grid-dist))
  )

  ; Expanding the rules start here.
  ; This rules are defined without recursion.
  (start G 99)
)
