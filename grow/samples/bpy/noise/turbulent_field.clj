(use 'grow.core) ; maps this grow DSL to Clojure
(template-bpy)   ; use a template suitable for Blenders Python API

(grow
  ; Some constants and a function to control the amount of noise.
  (def octaves   4)
  (def ampscale  0.85)
  (def freqscale 0.75)
  (def coordscale 1.3)
  (def turbscale 1.4)
  (defn turb [xyz]
    (- (turbulence xyz
                   coordscale turbscale
                   octaves ampscale freqscale) 0.65))
  ; Bail out value to stop the recursion
  (def recursion-depth 43)

  ; Recursively generate points, disturbed by Perlin turbulence.
  (rule Point
    (let [ay (turb [(coord-x) (coord-y) (coord-z)])
          ax (turb [(coord-z) (coord-x) (coord-y)])]
      (println (coord) ay ax) ; print line debugging
      (invoke Point
              :coord  [(rotate-y ay) (rotate-x ax) (translate 0 0 0.1)]
              :rotate [(rotate-y ay) (rotate-x ax)])
      (emit ["curve_point"]))
  )

  ; Generate start and stop sequence for a Bezier spline.
  (rule Curve
    (emit ["curve_base"]
          :scale [(scale 0.5)] ; make it not so fat
          :color (hsv (rnd-gaussian mPi) 0.7 0.9)) ; random color angle
    (invoke Point)
    (emit ["curve_end"])
  )

  ; Generate a 7 by 7 grid and start the curve for every grid point.
  (rule G
    (generator [Curve] (grid 7 7 1 0.1))
  )

  ; Expanding the rules start here and is limited to recursion-depth.
  (start G recursion-depth)
)
