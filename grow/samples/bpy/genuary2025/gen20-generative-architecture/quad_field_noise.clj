(use 'grow.core)
(template-bpy)

(grow
  (def len 14)
  (def p 0.5)
  (def m (- p))
  (def s 0.5)
  (def dz 0.1)
  (def sz 1.2)

  (rule O (emit ["cube"]))

  (rule C (invoke O :color (hsv (radian 30) 0.25 1)))

  (rule C (invoke O :color (hsv (radian (+ 180 30)) 0.25 1)))

  (rule D
    (invoke C)
    (invoke R :coord  [(translate p p dz) (scale s s sz)]
              :scale  [(scale s s sz)])
    (invoke R :coord  [(translate p m dz) (scale s s sz)]
              :scale  [(scale s s sz)])
    (invoke R :coord  [(translate m p dz) (scale s s sz)]
              :scale  [(scale s s sz)])
    (invoke R :coord  [(translate m m dz) (scale s s sz)]
              :scale  [(scale s s sz)])
  )

  (rule R
    (if (> 0.5 (noise (coord) 1 1))
      (invoke C)
      (invoke D))
  )

  (rule S (invoke R :scale  [(scale 1)]))

  (rule G (generator [S] (grid 5 5 1 2)))

  (start G len)
)
