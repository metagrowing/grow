(use 'grow.core)
(template-bpy)
(grow
  (rule C [treedepth]
    (emit ["cube"])
    (emit ["modifier"] {:bevel (* (Math/sqrt treedepth) 0.0005)})
  )

  (rule T [treedepth]
    (invoke TILE treedepth
                :coord [(scale 0.5 1 1) (translate 0.5 0 0)]
                :scale [(scale 0.5 1 1)])
    (invoke TILE treedepth
                :coord [(scale 0.5 1 1) (translate -0.5 0 0)]
                :scale [(scale 0.5 1 1)])
  )

  (rule T [treedepth]
    (invoke TILE treedepth
                :coord [(scale 1 0.5 1) (translate 0 0.5 0)]
                :scale [(scale 1 0.5 1)])
    (invoke TILE treedepth
                :coord [(scale 1 0.5 1) (translate 0 -0.5 0)]
                :scale [(scale 1 0.5 1)])
  )

  (rule TILE [treedepth]
    (if (pos? treedepth)
      (if (or (rnd-boolean) (rnd-boolean))
        (invoke T (dec treedepth))
        (invoke C treedepth))
      (comment "allow a gap"))
  )

  (rule ROOT
    (invoke TILE 17)
  )

  (start ROOT 999)
)
