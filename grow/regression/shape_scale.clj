(use 'grow.core)
(template-bpy)

(grow
  (rule C
    (invoke C :coord [(translate 1.5 0 0)]
              :scale [(scale 0.8)])
    (emit ["cube"])
  )
 
  (start C 5)
)
