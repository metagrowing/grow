(use 'grow.core)
(template-bpy)

(grow
  (def tx 1)

  (rule PPS [len next-color]
    (emit ["paint_point"])
    (if (pos? len)
      (invoke PPS (dec len) next-color
                :coord [(translate tx 0 0)]
                :color (next-color shape-color)))
  )

  (rule PS [len next-color]
    (emit ["paint_base"] { :smooth true })
    (invoke PPS len next-color
            :coord [(translate tx 0.25 0)])
    (emit ["paint_end"])
  )

  (rule PPH [len next-color]
    (emit ["paint_point"])
    (if (pos? len)
      (invoke PPH (dec len) next-color
                :coord [(translate tx 0 0)]
                :color (next-color shape-color)))
  )

  (rule PH [len next-color]
    (emit ["paint_base"] { :smooth false })
    (invoke PPH len next-color
            :coord [(translate tx -0.25 0)])
    (emit ["paint_end"])
  )

  (rule CC [len next-color]
    (emit ["cube"] :scale [(scale 0.125 0.25 0.1)])
    (if (pos? len)
      (invoke CC (dec len) next-color
                :coord [(translate tx 0 0)]
                :color (next-color shape-color)))
  )

  (rule C [len next-color]
    (invoke CC len next-color
            :coord [(translate tx 0 0)])
  )

;;   (rule L1 [len]
;;     (invoke P len (/ m2Pi (inc len)))
;;     (invoke C len (/ m2Pi (inc len)))
;;   )

;;   (rule L2 [len]
;;     (invoke C len (hue-func len))
;;     (invoke P len (hue-func len))
;;   )

  (rule L [len func]
    (invoke PS len (func len))
    (invoke C  len (func len))
    (invoke PH len (func len))
  )

  (defn hue-func [l]
        (let [d (/ m2Pi (inc l))]
              (fn [c] (h+ d c))))
  
  (defn sat-func [l]
        (let [d (/ 1 (inc l))]
              (fn [c] (s- d c))))
  
  (defn val-func [l]
        (let [d (/ 1 (inc l))]
              (fn [c] (v+ d c))))
  
  (rule S
    (invoke L 1 hue-func :coord [(translate 0  1 0)] :color (hsv 0.0 1.0 1.0))
    (invoke L 2 hue-func :coord [(translate 0  2 0)] :color (hsv 0.0 0.5 1.0))
    (invoke L 3 hue-func :coord [(translate 0  3 0)] :color (hsv 0.5 1.0 1.0))
    (invoke L 4 hue-func :coord [(translate 0  4 0)] :color (hsv 0.5 1.0 0.5))

    (invoke L 5 sat-func :coord [(translate 0  5 0)] :color cRed)
    (invoke L 6 sat-func :coord [(translate 0  6 0)] :color cYellow)
    (invoke L 7 sat-func :coord [(translate 0  7 0)] :color cGreen)
    (invoke L 8 sat-func :coord [(translate 0  8 0)] :color cCyan)
    (invoke L 9 sat-func :coord [(translate 0  9 0)] :color cBlue)
    (invoke L 0 sat-func :coord [(translate 0 10 0)] :color cMagneta)

    (invoke L 2 val-func :coord [(translate 0 11 0)] :color cBlack)
    (invoke L 7 val-func :coord [(translate 0 12 0)] :color cBlack)
  )

  (start S 99)
)
