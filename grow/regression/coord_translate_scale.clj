(use 'grow.core)
(template-bpy)

(grow
  (rule C
    (invoke C :coord [(translate 1.5 0 0) (scale 1.1)])
    (emit ["cube"])
  )
 
  (start C 5)
)
