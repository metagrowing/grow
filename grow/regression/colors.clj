(use 'grow.core)
(template-bpy)

(grow
  (def cDarkOrange (rgb 1.0000, 0.2456, 0.0000))
  (println cDarkOrange)

  (rule C [s n]
    (emit [s n])
  )

  (rule G
    (invoke C "cube" "cRed"        :coord [(translate 1 1 0)] :color cRed)
    (invoke C "cube" "cBlack"      :coord [(translate 2 1 0)] :color cBlack)
    (invoke C "cube" "cYellow"     :coord [(translate 3 1 0)] :color cYellow)
    (invoke C "cube" "cGreen"      :coord [(translate 1 2 0)] :color cGreen)
    (invoke C "cube" "cCyan"       :coord [(translate 2 2 0)] :color cCyan)
    (invoke C "cube" "cBlue"       :coord [(translate 3 2 0)] :color cBlue)
    (invoke C "cube" "cMagneta"    :coord [(translate 1 3 0)] :color cMagneta)
    (invoke C "cube" "cWhite"      :coord [(translate 2 3 0)] :color cWhite)
    (invoke C "cube" "cDarkOrange" :coord [(translate 3 3 0)] :color cDarkOrange)
  )
  
  (start G 2)
)

