(use 'grow.core)
(template-bpy)

(grow
  (rule C
    (emit ["cube"])
    (invoke C :coord [(translate 1.3 0 0)])
  )
 
  (start C 5)
)

