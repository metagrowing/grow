(use 'grow.core)
(template-bpy)

(grow
  (defn color-value [color] (nth color 2))
  (defn nonlinear [v] (* v v v 5))

  (rule C 
    (emit ["cube" "none_mat"]
                   :scale [(scale 1 1 (nonlinear (color-value shape-color)))])
  )
  (rule C 
    (emit ["cube"] :color "Metal"
                   :scale [(scale 1 1 (nonlinear (color-value shape-color)))])
    (emit ["modifier"]   {:smooth true :wireframe 0.15})
  )
  (rule C 
    (emit ["cube"] :color "Plastic"
                   :scale [(scale 1 1 (nonlinear (color-value shape-color)))])
    (emit ["modifier"] {:bevel 0.125 :segments 3})
  )
  (rule G 
    (emit ["material" "Metal"]   {:roughness 0.05 :metallic 0.9})
    (emit ["material" "Plastic"] :color cYellow
                                 {:roughness 0.5  :metallic 0.0 :clearcoat 0.1})
   (generator [C] (import-bitmap "~/grow/inp/smooth-turbulence-16x16.png"))
  )
  (start G 2)
)
