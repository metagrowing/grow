(use 'grow.core)
(template-bpy)

(grow
  (rule B
    (emit ["cube"] :scale [(scale 1 0.02 0.02)])
  )

  (rule C
    (emit ["cube"] :scale [(scale 0.01 0.2 0.2)])
    (invoke B)
  )

  (rule D 
    (generator [C] (sphere-fibonacci 125 1))
  )

  (rule G 
    (generator [D] (sphere-fibonacci 10 4))
  )

  (start G 999)
)
