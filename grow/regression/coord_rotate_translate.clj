(use 'grow.core)
(template-bpy)

(grow
  (rule C
    (invoke C :coord [(rotate-y mPi8) (translate 1.5 0 0)])
    (emit ["cube"])
  )
 
  (start C 5)
)

