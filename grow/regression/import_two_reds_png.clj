(use 'grow.core)
(template-bpy)

(grow
  (defn color-value [color] (nth color 2))
  (defn nonlinear [v] (* v v v 5))

  (rule C 
    (emit ["cube"]
      :scale [(scale 1 1 (nonlinear (color-value shape-color)))])
  )
  (rule G 
   (generator [C]
     (import-bitmap "~/grow/inp/electro-two-reds-113x62.png"))
  )
  (start G 999)
)
