(use 'grow.core)
(template-bpy)

(grow

  (rule B
    (emit ["cube"] :scale [(scale 2 0.05 0.05)])
  )

  (rule C
    (emit ["cube"] :coord [(translate (rnd-gaussian 0.25) 0 0)]
                   :scale [(scale 0.05 1 1)])
    (invoke B      :coord [(translate 1 0 0)])
  )

  (rule G 
    (generator [C] (sphere-random 500 10 0.1))
  )

  (start G 99)
)
