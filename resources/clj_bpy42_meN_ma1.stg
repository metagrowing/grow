principledInputMap ::= [
    ":metallic" : "1",
    ":roughness" : "2",
    ":ior" : "3",
    ":alpha" : "4",
    ":subsurface_weight" : "7",
    ":specular_ior" : "12",
    ":anisotropic" : "14",
    ":anisotropic_rotation" : "15",
    ":transmission" : "17",
    ":clearcoat" : "18",
    ":clearcoat_roughness" : "19",
    ":clearcoat_ior" : "20",
    ":sheen" : "23",
    ":sheen_roughness" : "24",
    ":emission" : "27",
    ":thinfilm" : "28",
    ":thinfilm_ior" : "29",
    default:"_!!error_"
]

container(traits, shapes) ::= <<
import sys
import time
import bpy
import bmesh
from bpy_extras.node_shader_utils import PrincipledBSDFWrapper

start_time = time.time()
true = True
false = not True

default_traits = { ':ico_subdivisions' : 2, ':cylinder_segments' : 12, }
global_traits = {<traits:expandModAttr(); separator=" ">}
traits = {**default_traits, **global_traits}

sce = bpy.context.scene
if not sce.render.engine == 'CYCLES':
    sce.render.engine = 'CYCLES'
#sce.cycles.device = 'CPU'
sce.cycles.feature_set = 'EXPERIMENTAL'

col = bpy.data.collections.new("Collection 2")
sce.collection.children.link(col)

mcu = bmesh.new()
bmesh.ops.create_cube(mcu, size=1)
mic = bmesh.new()
bmesh.ops.create_icosphere(mic, subdivisions=traits[':ico_subdivisions'], radius=1.0)
mcy = bmesh.new()
bmesh.ops.create_cone(mcy, cap_ends=True, segments=traits[':cylinder_segments'], radius1=1.0, radius2=1.0, depth=1.0)

bpy.ops.curve.primitive_bezier_curve_add()
taper = bpy.context.object 
taper.name = "TaperCr"
last_taper = None

bpy.ops.curve.primitive_bezier_circle_add()
obevel = bpy.context.object
obevel.name = 'BevelCr'
obevel.scale = (0.1, 0.1, 1)

obj = None

objmat = bpy.data.materials.new(name='Ma')
objmat.use_nodes = True
bsdf = PrincipledBSDFWrapper(objmat, is_readonly=False)
bsdf.base_color = (0.5, 0.5, 0.5)
nt = bsdf.material.node_tree
ninf = nt.nodes.new(type="ShaderNodeObjectInfo")
ninf.location.x = -170
ninf.location.y = 300
nt.links.new(ninf.outputs[1], nt.nodes["Principled BSDF"].inputs[0])

vxcmat = bpy.data.materials.new(name='MaVx')
vxcmat.use_nodes = True
bsdf = PrincipledBSDFWrapper(vxcmat, is_readonly=False)
bsdf.base_color = (0.5, 0.5, 0.5)
nt = bsdf.material.node_tree
nvxc = nt.nodes.new(type="ShaderNodeVertexColor")
nvxc.location.x = -170
nvxc.location.y = 150
nvxc.layer_name = "Col"
nt.links.new(nvxc.outputs[0], nt.nodes["Principled BSDF"].inputs[0])

mdict = {}
vxc = []
vxcsmooth = False

bpy.ops.object.select_all(action='DESELECT')

def a_cu(pref, seq, loc, sca, rot, rgb):
    <expandMesh()>
    mcu.to_mesh(me)
    <expandLSR()>
    <expandMat()>

def m_cu(pref, seq, loc, sca, rot, ref):
    <expandMesh()>
    mcu.to_mesh(me)
    <expandLSR()>
    if ref in mdict:
        obj.data.materials.append(mdict[ref] )

def a_ic(pref, seq, loc, sca, rot, rgb):
    <expandMesh()>
    mic.to_mesh(me)
    <expandLSR()>
    <expandMat()>

def m_ic(pref, seq, loc, sca, rot, ref):
    <expandMesh()>
    mic.to_mesh(me)
    <expandLSR()>
    if ref in mdict:
        obj.data.materials.append(mdict[ref] )

def a_cy(pref, seq, loc, sca, rot, rgb):
    <expandMesh()>
    mcy.to_mesh(me)
    <expandLSR()>
    <expandMat()>

def m_cy(pref, seq, loc, sca, rot, ref):
    <expandMesh()>
    mcy.to_mesh(me)
    <expandLSR()>
    if ref in mdict:
        obj.data.materials.append(mdict[ref] )

def curve_base(pref, attr, seq, loc, sca, rot, rgb, ref, usevxc):
    global obj
    li = pref + '_' + str(int(seq))
    bpy.ops.curve.primitive_bezier_curve_add()
    obj = bpy.context.object
    obj.name = li
    col.objects.link(obj)
    #col.objects.link(obevel)
    bpy.context.collection.objects.unlink(obj)
    <expandLSR()>
    if usevxc:
      <expandVertexMat()>
    else:
      if ref and ref in mdict:
          obj.data.materials.append(mdict[ref])
      else:
          <expandMat()>
    obj.data.bevel_object = obevel
    #if ':bevel' in attr:
    #    sxy = attr[':bevel']
    #    obevel.scale = (sxy, sxy, 1)
    if ':bevel' in attr:
        obj.data.bevel_object = obevel
    if ':taper' in attr:
      obj.data.taper_object = taper
    if last_taper is not None:
      obj.data.taper_object = last_taper
    bpy.ops.object.editmode_toggle()
    bpy.ops.curve.delete(type='SEGMENT')

def rgbmerge(mix, c1, c2):
    mix2 = max(min(mix, 1.0), 0.0)
    mix1 = 1-mix2
    return ( mix1 * c1[0] + mix2 * c2[0], mix1 * c1[1] + mix2 * c2[1], mix1 * c1[2] + mix2 * c2[2], 1 )

def paint_end():
    <curveStop()>
    bpy.ops.object.convert(target='MESH')
    vxmesh = obj.data
    if not vxmesh.vertex_colors:
        vxmesh.vertex_colors.new()
    color_layer = vxmesh.vertex_colors["Col"]
    polyindex = 0
    nrings = 12 # assumes there are 12 face loops per segment TODO
    nverts = 48 # assumes there are 48 vertices per face per ring TODO
    if vxcsmooth:
        for vxpoly in vxmesh.polygons:
            ringidx =  polyindex // nverts
            vxcidx = ringidx // nrings
            vxcmod = ringidx % nrings
            if len(vxpoly.loop_indices) == 4:
                color_layer.data[vxpoly.loop_start+0].color = rgbmerge(vxcmod / nrings,            vxc[vxcidx % len(vxc)], vxc[(vxcidx + 1) % len(vxc)])
                color_layer.data[vxpoly.loop_start+1].color = rgbmerge(vxcmod / nrings + 1.0 / nrings, vxc[vxcidx % len(vxc)], vxc[(vxcidx + 1) % len(vxc)])
                color_layer.data[vxpoly.loop_start+2].color = rgbmerge(vxcmod / nrings + 1.0 / nrings, vxc[vxcidx % len(vxc)], vxc[(vxcidx + 1) % len(vxc)])
                color_layer.data[vxpoly.loop_start+3].color = rgbmerge(vxcmod / nrings,            vxc[vxcidx % len(vxc)], vxc[(vxcidx + 1) % len(vxc)])
            else:
              for loopidx in vxpoly.loop_indices:
                  unicolor = rgbmerge(vxcmod / nrings, vxc[vxcidx % len(vxc)], vxc[(vxcidx + 1) % len(vxc)])
                  color_layer.data[loopidx].color = unicolor
            polyindex += 1
    else:
        for vxpoly in vxmesh.polygons:
            ringidx =  polyindex // nverts
            vxcidx = ringidx // nrings
            for loopidx in vxpoly.loop_indices:
                unicolor = vxc[vxcidx % len(vxc)]
                color_layer.data[loopidx].color = vxc[vxcidx % len(vxc)]
            polyindex += 1

def taper_base(pref, attr, seq, loc, sca, rot, rgb, ref):
    global obj
    li = pref + '_' + str(int(seq))
    bpy.ops.curve.primitive_bezier_curve_add()
    obj = bpy.context.object
    obj.name = li
    col.objects.link(obj)
    bpy.context.collection.objects.unlink(obj)
    <expandLSR()>
    bpy.ops.object.editmode_toggle()
    bpy.ops.curve.delete(type='SEGMENT')


#shapes
try:
    <shapes:expandShape(); separator="\n">
    print(time.time() - start_time)
except:
    print(sys.exc_info()[1])
    import traceback
    traceback.print_tb(sys.exc_info()[2])
    bpy.ops.object.text_add()
    oerr = bpy.context.object
    oerr.name = '!Error'
    oerr.data.body = str(sys.exc_info()[1])

mcu.free()
mic.free()
mcy.free()
vcx = []

layer = bpy.context.view_layer
layer.update()
bpy.ops.object.select_all(action='DESELECT')
bpy.ops.object.select_by_type(extend=False, type='MESH')
bpy.ops.object.select_by_type(extend=True, type='CURVE')
#bpy.ops.object.collection_objects_select()
bpy.ops.view3d.camera_to_view_selected()
bpy.ops.object.transform_apply(location=False, rotation=False, scale=True)
print(time.time() - start_time)
#end

>>

expandMesh() ::= <<
global obj
li = pref + '_' + str(int(seq))
me = bpy.data.meshes.new('Me' + li)
obj = bpy.data.objects.new(li, me)
col.objects.link(obj)
>>

expandLSR() ::= <<
obj.location = loc
obj.scale = sca
obj.rotation_mode = 'ZYX'
obj.rotation_euler = rot
>>

expandMat() ::= <<
obj.color = (rgb[0], rgb[1], rgb[2], 1)
if objmat is not None:
    obj.data.materials.append(objmat)
>>

expandVertexMat() ::= <<
if vxcmat is not None:
    obj.data.materials.append(vxcmat)
>>

expandShape(shape) ::= <<
<shape>
>>

modifier(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
li = <if(pref)>'<pref>'<else>'Mod'<endif> + '_<seq>'
mi = 0
attr = { <attr:expandModAttr(); separator="\n"> }
if ':subsurf' in attr:
    obj.modifiers.new(li + '_'  + str(mi), 'SUBSURF')
    obj.modifiers[mi].levels = attr[':subsurf']
    obj.modifiers[mi].render_levels = attr[':subsurf']
    mi = mi + 1
if ':wireframe' in attr:
    obj.modifiers.new(li + '_'  + str(mi), 'WIREFRAME')
    obj.modifiers[mi].thickness = attr[':wireframe']
    obj.modifiers[mi].offset = 1
    mi = mi + 1
if ':bevel' in attr or ':segments' in attr:
    obj.modifiers.new(li + '_'  + str(mi), 'BEVEL')
    if ':bevel' in attr:
        obj.modifiers[mi].width = attr[':bevel']
    if ':segments' in attr:
        obj.modifiers[mi].segments = attr[':segments']
>>

shade(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
attr = { <attr:expandModAttr(); separator="\n"> }
if ':smooth' in attr and attr[':smooth']:
    obj.select_set(True)
    bpy.ops.object.shade_smooth()
    obj.select_set(False)
>>

curveStop() ::= <<
bpy.ops.curve.switch_direction()
bpy.ops.curve.select_all(action='SELECT')
bpy.ops.curve.normals_make_consistent()
bpy.ops.object.editmode_toggle()
>>

expandAttr(attr) ::= <<
ins[<principledInputMap.(first(attr))>].default_value = <rest(attr)>
>>

expandModAttr(attr) ::= <<
'<first(attr)>' : <rest(attr)>,
>>

expandCustom(attr) ::= <<
obj['<first(attr)>'] = <rest(attr)>
>>

material(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
mat = bpy.data.materials.new(name=<if(pref)>'<pref>'<else>'Ma'<endif>)
mat.use_nodes = True
bsdf = PrincipledBSDFWrapper(mat, is_readonly=False)
<if(r)>bsdf.base_color = (<r>, <g>, <b>)<endif>
ins = bsdf.node_principled_bsdf.inputs
<attr:expandAttr(); separator="\n">
mdict[<if(pref)>'<pref>'<else>'Ma'<endif>] = mat
>>

cube(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<if(r)>a_cu(<if(pref)>'<pref>'<else>'Cu'<endif>, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>))<\\>
<else>m_cu(<if(pref)>'<pref>'<else>'Cu'<endif>, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), '<ref>')<\\>
<endif>
>>

sphere(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<if(r)>a_ic(<if(pref)>'<pref>'<else>'Ic'<endif>, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>))<\\>
<else>m_ic(<if(pref)>'<pref>'<else>'Ic'<endif>, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), '<ref>')<\\>
<endif>
<attr:expandCustom(); separator="\n">
<attr:expandAttr(); separator="\n">
>>

cylinder(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
a_cy(<if(pref)>'<pref>'<else>'Cy'<endif>, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>))
<attr:expandCustom(); separator="\n">
<attr:expandAttr(); separator="\n">
>>

curve_base(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<if(r)>curve_base(<if(pref)>'<pref>'<else>'Cr'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>), '<ref>', False)<\\>
<else>curve_base(<if(pref)>'<pref>'<else>'Cr'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (1, 1, 1), '<ref>', False)<\\>
<endif>
>>

curve_point(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
bpy.ops.curve.vertex_add(location=(<x>, <y>, <z>))
>>

curve_end(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<curveStop()>
>>

taper_base(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<if(r)>taper_base(<if(pref)>'<pref>'<else>'Tc'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>), '<ref>')<\\>
<else>taper_base(<if(pref)>'<pref>'<else>'Tc'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (1, 1, 1), '<ref>')<\\>
<endif>
>>

taper_point(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
bpy.ops.curve.vertex_add(location=(<x>, <y>, 0))
>>

taper_end(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
<curveStop()>
last_taper = bpy.context.object
>>


paint_base(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
vxc = []
vxcattr = { <attr:expandModAttr(); separator=" "> }
vxcsmooth = (':smooth' in vxcattr) and vxcattr[':smooth']
<if(r)>curve_base(<if(pref)>'<pref>'<else>'Cr'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (<r>, <g>, <b>), '<ref>', True)<\\>
<else>curve_base(<if(pref)>'<pref>'<else>'Cr'<endif>, { <attr:expandModAttr(); separator=" "> }, <seq>, (<x>, <y>, <z>), (<sx>, <sy>, <sz>), (<rx>, <ry>, <rz>), (1, 1, 1), '<ref>', True)<\\>
<endif>
>>

paint_point(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
bpy.ops.curve.vertex_add(location=(<x>, <y>, <z>))
vxc.append( (<r>, <g>, <b>, 1.0) )
>>

paint_end(pref, seq, depth, attr, x, y, z, sx, sy, sz, rx, ry, rz, r, g, b, ref) ::= <<
paint_end()
>>
